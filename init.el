  (set-default-coding-systems 'utf-8)     ; Default to utf-8 encoding
  (prefer-coding-system       'utf-8)     ; Add utf-8 at the front for automatic detection.
  (set-terminal-coding-system 'utf-8)     ; Set coding system of terminal output
  (set-keyboard-coding-system 'utf-8)     ; Set coding system for keyboard input on TERMINAL
  (set-language-environment "English")    ; Set up multilingual environment
  (prefer-coding-system 'utf-8)
  (set-default-coding-systems 'utf-8)
  (set-terminal-coding-system 'utf-8)
  (set-keyboard-coding-system 'utf-8)
  (set-selection-coding-system 'utf-8)
  (set-file-name-coding-system 'utf-8)
(set-clipboard-coding-system 'utf-8)
(if (eq system-type 'windows-nt)
    (progn
        (set-w32-system-coding-system 'utf-8)))
  (set-buffer-file-coding-system 'utf-8)

(org-babel-load-file "~/.emacs.d/configuration.org")
